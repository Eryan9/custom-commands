Hooks.once("init", async function () {
	// Rend accessible les nouvelles commandes macro
	game.customCommands = CustomCommands;
});

class CustomCommands{
	
	static showTile(image, locked, sizeRatio, sound, volume, delayBeforeFadeOut, animationFPS, fadeOutDuration){
		
		const fadeOutTick = Math.floor(1000/animationFPS);
		let img = new Image();
		img.onload = function() {
			const originalSize = { width:this.width, height:this.height }; 
			
			// Récupération de la scène actuellement affichée
			let scene = Scene.collection.viewed.data;

			// Calcul de la taille de la tuile pour prendre en compte le ratio sans déborder 
			var size = {};
			if ((scene.width >= scene.height && originalSize.width >= originalSize.height) 
				|| (scene.width < scene.height && originalSize.width > originalSize.height ) ){
				
				size.width = Math.floor(scene.width * sizeRatio);
				size.height = Math.floor(size.width * (originalSize.height / originalSize.width));
			}
			else
			{
				size.height = Math.floor(scene.height * sizeRatio);
				size.width = Math.floor(size.height * (originalSize.height / originalSize.width));
			}

			const tileSize = size;
			const ratio = tileSize.width / tileSize.height;

			// Détermination de la position de la tuile
			let position = {};
			position.x = Math.floor((scene.width / 2) - (size.width / 2 ) + (scene.padding * scene.width) );
			position.y = Math.floor((scene.height / 2) - (size.height / 2 ) + (scene.padding * scene.height) );

			// Création de la tuile
			var tile;
			Tile.create({
				img:image,
				x:position.x,
				y:position.y,
				width:size.width,
				height:size.height,
				locked:locked,
			}).then((newTile)=>{ tile = newTile; });

			// Lecture du fichier audio si il y en as un de fourni
			if (sound) AudioHelper.play({src: sound, volume: volume, autoplay: true, loop: false}, true);

			// Déclenchement de retrécissement de la tuile après le délai défini
			setTimeout(()=>{
				fadeOut();
			}, delayBeforeFadeOut * 1000);
			
			// Fonction récursive de retrécissement
			function fadeOut(){
				setTimeout(()=>{
					// calcul des nouvelles largeur et hauteur
					let width = Math.max(0, tile.data.width - ( fadeOutTick * tileSize.width / (fadeOutDuration*1000) ) );
					let height = Math.floor(width / ratio);
					
					// Calcul de la nouvelle position
					let position = {};
					position.x = Math.floor((scene.width / 2) - (width / 2 ) + (scene.padding * scene.width) );
					position.y = Math.floor((scene.height / 2) - (height / 2 ) + (scene.padding * scene.height) );
					
					// Si le largeur est encore supérieur à 0, on met à jour la tuile
					if (width > 0){
						tile.update({
							x:position.x,
							y:position.y,
							width:width, 
							height:height
						});   
						// Relance la fonction récursive après le délai (tick) défini     
						fadeOut(tile);
					} 
					// Sinon, si la largeur est égale à 0, on supprime la tuile
					else tile.delete();
				},fadeOutTick);
			};	
			
		}
		img.src = image;
	}
}