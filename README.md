**Ajout de nouvelles commandes pour les macros dans Foundry VTT**  

***showTile***  
Cette commande permet d'afficher une tuile, contenant l'image que l'on souhaite, au milieu de la scène active et lance un son  
Puis fait disparaître la tuile avec une animation de retrécissement  

**Commande** :  
game.customCommands.showTile(image, locked, sizeRatio, sound, volume, delayBeforeFadeOut, animationFPS, fadeOutDuration);  

**paramètres:**  
- image : Url de l'image à afficher  
- locked : la tuile doit-elle être vérouiller ? empêche qu'elle soit déplacer par inadvertance  
- sizeRatio : Dimension de la tuile en pourcentage de la taille de la scène de 0 (0%) à 1 (100%)  
- sound : url du fichier son à jouer à l'affichage de la tuile  
- volume : volume du son de 0 (0%) à 1 (100%)  
- delayBeforeFadeOut : délai en seconde avant de commencer à faire disparaître la tuile (valeurs flottantes autorisés, ex : 1.5)  
- animationFPS : Image par seconde de l'animation de retrécissement (plus la valeur est élevée, plus ce sera fluide mais plus ça demandera des ressources)  
- fadeOutDuration : Temps totale pour faire disparaître la tuile en seconde (valeurs flottantes autorisés, ex : 0.5)  

**exemple :**  
game.customCommands.showTile("worlds/cof_initiation/objection2.png",true, 0.8, "/systems/cof/sounds/gulp.mp3", 0.8, 1, 50, 0.5);  
